//
//
//  LGProgressHUD.m
//  localSVProgressHUD
//
//  Created by Leopoldo Gomez on 08/02/2013.
//  Copyright (c) 2013 Leopoldo Gomez. All rights reserved.
//

#import "LGProgressHUD.h"

@interface LGProgressHUD ()

@end

@implementation LGProgressHUD

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(detectOrientation) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void) showWithStatus:(NSString *)messageText{
    
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    CGPoint loadingIndicatorPosition = CGPointMake(0, 0);
    
    beingShown = true ;
    if (self.localProgressHUDView != nil) {
        [self.localProgressHUDView removeFromSuperview] ;
    }
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    self.myImage = [[UIImageView alloc] init ] ;
    self.myImage.image = [UIImage imageNamed:@"LGProgressHUD.bundle/success.png"] ;
    self.myImage.frame = CGRectMake(0, 0, 30 , 30) ;
    self.myImage.center = self.loadingIndicator.center ;
    [self.localProgressHUDView addSubview: self.myImage ] ;
    [self.loadingIndicator stopAnimating] ;
    [self.loadingIndicator removeFromSuperview] ;
    
    CGPoint imagePosition = CGPointMake(70, 37) ;
    self.myImage.center = imagePosition ;
    self.myImage.alpha = 0.0 ;
    
    // Portrait
    if(orientation ==  UIInterfaceOrientationPortrait){
        
        NSLog(@"UIInterfaceOrientationPortrait") ;
        
        self.localProgressHUDView = [[UIView alloc] init ] ;
        self.localProgressHUDView.frame = CGRectMake(mainWindow.center.x, mainWindow.center.y, 135 , 100 ) ;
        
        CGPoint screenCentre = CGPointMake( mainWindow.center.x , mainWindow.center.y - mainWindow.center.y*0.2 );
        self.localProgressHUDView.center = screenCentre ;
        self.localProgressHUDView.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.90] ;
        self.localProgressHUDView.layer.borderWidth     = 0 ;
        self.localProgressHUDView.layer.cornerRadius    = self.localProgressHUDView.bounds.size.height/14;
        
        [self.view addSubview:self.localProgressHUDView];
        
        self.textLabel = [[UILabel alloc ] initWithFrame:CGRectMake(0, 10, 120, 44)];
        self.textLabel.center = CGPointMake(67, 72);
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.textLabel.numberOfLines = 1;
        self.textLabel.textColor = [UIColor whiteColor];
        self.textLabel.backgroundColor = [UIColor clearColor];
        
        // ipad
        loadingIndicatorPosition = CGPointMake(70, 37);
        self.myImage.center = CGPointMake(70, 37);
        
        self.loadingIndicator = [[UIActivityIndicatorView alloc]
                                 initWithActivityIndicatorStyle:
                                 UIActivityIndicatorViewStyleWhiteLarge];
    }
    
    // Portrait Upside Down
    if(orientation ==  UIInterfaceOrientationPortraitUpsideDown){
        
        NSLog(@"UIInterfaceOrientationPortraitUpsideDown") ;
        
        self.localProgressHUDView = [[UIView alloc] init ] ;
        self.localProgressHUDView.frame = CGRectMake(mainWindow.center.x, mainWindow.center.y, 135 , 100 ) ;
        
        CGPoint screenCentre = CGPointMake( mainWindow.center.x , mainWindow.center.y + mainWindow.center.y*0.2 );
        self.localProgressHUDView.center = screenCentre ;
        self.localProgressHUDView.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.90] ;
        self.localProgressHUDView.layer.borderWidth     = 0 ;
        self.localProgressHUDView.layer.cornerRadius    = self.localProgressHUDView.bounds.size.height/14;
        
        [self.view addSubview:self.localProgressHUDView];
        
        self.textLabel = [[UILabel alloc ] initWithFrame:CGRectMake(0, 100, 120, 44)];
        self.textLabel.center = CGPointMake(67, 72-47);
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.textLabel.numberOfLines = 1;
        self.textLabel.textColor = [UIColor whiteColor];
        self.textLabel.backgroundColor = [UIColor clearColor];
        
        // ipad
        loadingIndicatorPosition = CGPointMake(70, 37+25);
        self.myImage.center = CGPointMake(70, 37+25);
        
        self.loadingIndicator = [[UIActivityIndicatorView alloc]
                                 initWithActivityIndicatorStyle:
                                 UIActivityIndicatorViewStyleWhiteLarge];
        
        self.textLabel.transform = CGAffineTransformMakeRotation (M_PI) ;
    }
    
    
    
    if ( orientation ==  UIInterfaceOrientationLandscapeLeft) {
        NSLog(@"UIInterfaceOrientationLandscapeLeft") ;
        
        self.localProgressHUDView = [[UIView alloc] init ] ;
        self.localProgressHUDView.frame = CGRectMake(0, 0, 100 , 135 ) ;
        
        CGPoint screenCentre = CGPointMake( mainWindow.center.x - mainWindow.center.x*0.15  , mainWindow.center.y );
        self.localProgressHUDView.center = screenCentre ;
        self.localProgressHUDView.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.90] ;
        self.localProgressHUDView.layer.borderWidth     = 0 ;
        self.localProgressHUDView.layer.cornerRadius    = self.localProgressHUDView.bounds.size.height/14;
        
        [self.view addSubview:self.localProgressHUDView];
        
        self.textLabel = [[UILabel alloc ] initWithFrame:CGRectMake(0, 10, 120, 44)];
        self.textLabel.center = CGPointMake(67-40   +47 , 72-4);
        
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.textLabel.numberOfLines = 1;
        self.textLabel.textColor = [UIColor whiteColor];
        self.textLabel.backgroundColor = [UIColor clearColor];
        
        
        // iPad:
        loadingIndicatorPosition = CGPointMake(60 - 25 , 67);
        self.myImage.center = CGPointMake(60 - 25 , 67);
        
        self.loadingIndicator = [[UIActivityIndicatorView alloc]
                                 initWithActivityIndicatorStyle:
                                 UIActivityIndicatorViewStyleWhiteLarge];
        
        
        self.textLabel.transform = CGAffineTransformMakeRotation (M_PI*1.5) ;
    }
    
    
    if ( orientation ==  UIInterfaceOrientationLandscapeRight ) {
        
        NSLog(@"UIInterfaceOrientationLandscapeRight") ;
        
        self.localProgressHUDView = [[UIView alloc] init ] ;
        self.localProgressHUDView.frame = CGRectMake(0, 0, 100 , 135 ) ;
        
        CGPoint screenCentre = CGPointMake( mainWindow.center.x + mainWindow.center.x*0.15  , mainWindow.center.y );
        self.localProgressHUDView.center = screenCentre ;
        self.localProgressHUDView.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.90] ;
        self.localProgressHUDView.layer.borderWidth     = 0 ;
        self.localProgressHUDView.layer.cornerRadius    = self.localProgressHUDView.bounds.size.height/14;
        
        [self.view addSubview:self.localProgressHUDView];
        
        self.textLabel = [[UILabel alloc ] initWithFrame:CGRectMake(0, 10, 120, 44)];
        self.textLabel.center = CGPointMake(67-40, 72-4);
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.textLabel.numberOfLines = 1;
        self.textLabel.textColor = [UIColor whiteColor];
        self.textLabel.backgroundColor = [UIColor clearColor];
        
        
        // iPad:
        loadingIndicatorPosition = CGPointMake(60, 67);
        self.myImage.center = CGPointMake(60, 67);
        
        self.loadingIndicator = [[UIActivityIndicatorView alloc]
                                 initWithActivityIndicatorStyle:
                                 UIActivityIndicatorViewStyleWhiteLarge];
        
        
        self.textLabel.transform = CGAffineTransformMakeRotation (M_PI/2) ;
        
    }
    
    
    
    
	self.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size: 17.0];
    
	[self.localProgressHUDView addSubview:self.textLabel];
	self.textLabel.text = messageText;
    
	
    
	self.loadingIndicator.center = loadingIndicatorPosition;
	self.loadingIndicator.hidesWhenStopped = YES;
	[self.localProgressHUDView addSubview:self.loadingIndicator];
	[self.loadingIndicator startAnimating];
    
    [self.view bringSubviewToFront:self.localProgressHUDView] ;
    [mainWindow addSubview: self.localProgressHUDView];
    
    
    // Animate view:
    self.localProgressHUDView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.localProgressHUDView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        
        
    }];
    
    //[self detectOrientation] ;
    
    
    
    
}

- (void) dismissLocalProgressHUDView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.localProgressHUDView.transform = CGAffineTransformMakeScale(0.4, 0.4);
        self.localProgressHUDView.alpha = 0.0 ;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [self.localProgressHUDView removeFromSuperview] ;
        beingShown = false ;
    }];
    
}






- (void) showSuccessWithStatus: (NSString *)message {
    
    if (beingShown) {
        self.textLabel.text = message ;
        self.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size: 17.0];
        
        self.myImage = [[UIImageView alloc] init ] ;
        self.myImage.image = [UIImage imageNamed:@"LGProgressHUD.bundle/success.png"] ;
        self.myImage.frame = CGRectMake(0, 0, 30 , 30) ;
        self.myImage.center = self.loadingIndicator.center ;
        [self.localProgressHUDView addSubview: self.myImage ] ;
        [self.loadingIndicator stopAnimating] ;
        [self.loadingIndicator removeFromSuperview] ;
        
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        
        
        if ( orientation ==  UIInterfaceOrientationPortraitUpsideDown) {
            self.myImage.transform = CGAffineTransformMakeRotation (M_PI) ;
        }
        
        if ( orientation ==  UIInterfaceOrientationLandscapeRight) {
            self.myImage.transform = CGAffineTransformMakeRotation (M_PI/2) ;
        }
        
        if ( orientation ==  UIInterfaceOrientationLandscapeLeft) {
            self.myImage.transform = CGAffineTransformMakeRotation (M_PI*1.5) ;
        }
        
        
        UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
        [mainWindow bringSubviewToFront: self.localProgressHUDView];
        
        [self performSelector:@selector(dismissLocalProgressHUDView) withObject:nil afterDelay:1.0];
    }
    
}


- (void) showErrorWithStatus: (NSString *)message {
    
    if (beingShown) {
        self.textLabel.text = message ;
        self.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size: 17.0];
        
        self.myImage = [[UIImageView alloc] init ] ;
        self.myImage.image = [UIImage imageNamed:@"LGProgressHUD.bundle/error.png"] ;
        self.myImage.frame = CGRectMake(0, 0, 30 , 30) ;
        self.myImage.center = self.loadingIndicator.center ;
        [self.localProgressHUDView addSubview: self.myImage ] ;
        [self.loadingIndicator stopAnimating] ;
        [self.loadingIndicator removeFromSuperview] ;
        
        UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
        [mainWindow bringSubviewToFront: self.localProgressHUDView];
        
        [self performSelector:@selector(dismissLocalProgressHUDView) withObject:nil afterDelay:1.0];
    }
    
}


- (void) detectOrientation{
    
    
    if (false) {
        
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        
        
        if(orientation == UIInterfaceOrientationPortrait){
            NSLog(@"UIInterfaceOrientationPortrait") ;
        }
        
        if(orientation == UIInterfaceOrientationPortraitUpsideDown){
            NSLog(@"UIInterfaceOrientationPortraitUpsideDown") ;
            
        }
        
        
        
        if (orientation == UIInterfaceOrientationLandscapeLeft) {
            NSLog(@"UIInterfaceOrientationLandscapeLeft") ;
        }
        
        
        if(orientation == UIInterfaceOrientationLandscapeRight){
            NSLog(@"UIInterfaceOrientationLandscapeRight") ;
        }
        
    }
    
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    
    return YES;
}



- (BOOL)shouldAutorotate {
    
	return YES;
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll; // etc
}


// ios 5

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    NSLog(@"didRotateFromInterfaceOrientation ") ;
    
}

@end
