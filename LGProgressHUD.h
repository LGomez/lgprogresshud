//
//  LGProgressHUD.h
//  localSVProgressHUD
//
//  Created by Leopoldo Gomez on 08/02/2013.
//  Copyright (c) 2013 Leopoldo Gomez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface LGProgressHUD : UIViewController{
    bool beingShown ;
}


@property (strong, nonatomic) UIActivityIndicatorView * loadingIndicator ;
@property (strong, nonatomic) UIView * localProgressHUDView;
@property (strong, nonatomic) UILabel * textLabel ;
@property (strong, nonatomic) UIImageView * myImage ;


- (void) showWithStatus:(NSString *)messageText ;
- (void) dismissLocalProgressHUDView ;
- (void) showSuccessWithStatus: (NSString *)message ;
- (void) showErrorWithStatus: (NSString *)message ;



@end
