//
//  LGProgressHUD.h
//  localSVProgressHUD
//
//  Created by Leopoldo Gomez on 08/02/2013.
//  Copyright (c) 2013 Leopoldo Gomez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface LGProgressHUD : UIViewController{

}




@property (strong, nonatomic) IBOutlet UIView * localProgressHUDView;


- (void) showWithStatus:(NSString *)messageText ;

- (void) dismissLocalProgressHUDView ;

- (void) showSuccessWithStatus: (NSString *)message ;



@end
