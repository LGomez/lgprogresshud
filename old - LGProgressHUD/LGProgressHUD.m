//
//  LGProgressHUD.m
//  localSVProgressHUD
//
//  Created by Leopoldo Gomez on 08/02/2013.
//  Copyright (c) 2013 Leopoldo Gomez. All rights reserved.
//

#import "LGProgressHUD.h"

@interface LGProgressHUD ()

@end

@implementation LGProgressHUD

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void) showWithStatus:(NSString *)messageText{
    
    self.localProgressHUDView = [[UIView alloc] init ] ;
    self.localProgressHUDView.frame = CGRectMake(0, 0, 135, 100) ;
    CGPoint screenCentre = CGPointMake( self.view.center.x , self.view.center.y - self.view.center.y*0.2 );
    self.localProgressHUDView.center = screenCentre ;
    self.localProgressHUDView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7] ;
    self.localProgressHUDView.layer.borderWidth     = 0 ;
    self.localProgressHUDView.layer.cornerRadius    = self.localProgressHUDView.bounds.size.height/8;
    
	[self.view addSubview:self.localProgressHUDView];
    
	UILabel * loadingText = [[UILabel alloc ] initWithFrame:CGRectMake(0, 10, 120, 44)];
    loadingText.center = CGPointMake(70, 72);
	loadingText.textAlignment = NSTextAlignmentCenter;
	loadingText.lineBreakMode = NSLineBreakByWordWrapping;
	loadingText.numberOfLines = 1;
	loadingText.textColor = [UIColor whiteColor];
	loadingText.backgroundColor = [UIColor clearColor];
	loadingText.font = [UIFont fontWithName:@"Helvetica-Bold" size: 15.0];
	[self.localProgressHUDView addSubview:loadingText];
	loadingText.text = messageText;
    
	
    
	CGPoint loadingIndicatorPosition = CGPointMake(70, 37);
    UIActivityIndicatorView * loadingIndicator = [[UIActivityIndicatorView alloc]
                                                  initWithActivityIndicatorStyle:
                                                  UIActivityIndicatorViewStyleWhiteLarge];
	loadingIndicator.center = loadingIndicatorPosition;
	loadingIndicator.hidesWhenStopped = YES;
	[self.localProgressHUDView addSubview:loadingIndicator];
	[loadingIndicator startAnimating];
    
    [self.view bringSubviewToFront:self.localProgressHUDView] ;
    
    // Animate view:
    self.localProgressHUDView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.localProgressHUDView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        
    }];
    
}

- (void) dismissLocalProgressHUDView{
    
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        self.localProgressHUDView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        self.localProgressHUDView.alpha = 0.0 ;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [self.localProgressHUDView removeFromSuperview] ;
    }];
    
}

- (void) showSuccessWithStatus: (NSString *)message {
    
    
}





@end
