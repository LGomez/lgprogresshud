//
//  ViewController.m
//  localSVProgressHUD
//
//  Created by Leopoldo Gomez on 08/02/2013.
//  Copyright (c) 2013 Leopoldo Gomez. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController




- (IBAction)stopButtonPressed:(id)sender {
    [myLGProgressHUD dismissLocalProgressHUDView] ;
}



- (IBAction)buttonPressed:(id)sender {
    [myLGProgressHUD showWithStatus:@"Sending"] ;
}


- (IBAction)errorButtonPressed:(id)sender {
    [myLGProgressHUD showErrorWithStatus:@"Error"] ;
}



- (IBAction)successButtonPressed:(id)sender {
    [myLGProgressHUD showSuccessWithStatus:@"Success!"] ;
}




- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    myLGProgressHUD = [[LGProgressHUD alloc] init ] ;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return YES;
}


//==============


- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    NSLog(@" will rotate!" ) ;
}



@end
