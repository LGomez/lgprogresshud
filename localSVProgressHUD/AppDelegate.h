//
//  AppDelegate.h
//  localSVProgressHUD
//
//  Created by Leopoldo Gomez on 08/02/2013.
//  Copyright (c) 2013 Leopoldo Gomez. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
